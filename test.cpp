#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include <sstream>
#include <string>
#include <random>
#include <time.h>
#include <windows.h>
#include <algorithm>

using namespace std;

//generate the weight between -1 to 1 
double weightRand() {
  return ((float(rand()) / float(RAND_MAX)) * (1 - (-1))) +(-1);
}

float sigmoid(float value){

    return 1 / (1 + exp(-1 * value));
   
    
}

float sigmoid_derivative(float value){
    //sig * (1- sig)
	return sigmoid(value) * (1 - sigmoid(value));
}

void Vec_Init(int size, float *x)
{
	int i;
	for(i = 0; i < size; i++)
		x[i] = 1;
}

void Bias_Init(int size, float *x)
{
	int i;
	for(i = 0; i < size; i++)
		x[i] = 1;
}

/* dot product: return the dot product of vectors x and y */
float Dot_Product(int size, float *x, float *y)
{
	int i;
	double sum = 0.0;
	for(i = 0; i < size; i++)
		sum += x[i] * y[i];
	return (float)sum;
}

/* add two vectors */
//x y are input, z is output
void Vec_Add(int size, float *x, float *y, float *z)
{
	int i;
	for(i = 0; i < size; i++)
		z[i] = x[i] + y[i];
}

/* sub two vectors */
//x and y are input, z is output
void Vec_Sub(int size, float *x, float *y, float *z)
{
	int i;
	for(i = 0; i < size; i++)
		z[i] = (x[i] - y[i]);
}

/* display a vector */
void Vec_Show(int size, float *x)
{
	int i;

	std::cout << "vector size = " << size << std::endl;
	for (i = 0; i < size; i++)
		std::cout << x[i] << " ";
	std::cout << std::endl;
}

void Mat_Init(int row, int col, float *X)
{
	int i, size;

	size = row * col;
	for(i = 0; i < size; i++)
		X[i] = weightRand();
}

void Mat_Show(int row, int col, float *X)
{
	int i, j;
	std::cout << "row = " << row << " col = " << col << std::endl;
	for(i = 0; i < row; i++) {
		for(j = 0; j < col; j++)
			std::cout << X[col * i + j];
		std::cout << std::endl;
	}
}


void Mat_Xv(int row, int col, float *X, float *Y, float *v)
{
	int i, j;
	double result;

	for(i = 0; i < row; i++) {
		result = 0;
		for(j = 0; j < col; j++)
			result += v[j] * X[col * i + j];
		Y[i] = result;
	}
}

int main(int argc, const char * argv[]) {

	vector< vector<float> > X_train;
	vector<float> y_train;

	ifstream myfile("train_small.txt");

	if (myfile.is_open())
	{
		cout << "Loading data ...\n";
		string line;
		while (getline(myfile, line))
		{
			int x, y;
			vector<float> X;
			stringstream ss(line);
			ss >> y;
			y_train.push_back(y);
			for (int i = 0; i < 28 * 28; i++) {
				ss >> x;
				X.push_back(x/255.0);
			}
			X_train.push_back(X);
		}

		myfile.close();
		cout << "Loading data finished.\n";
	} 
	else 
		cout << "Unable to open file" << '\n';

        //transfer result to 0 and 1
    vector<vector<float>> correct_result;
	vector<float> temp_result;

	for(int i = 0;i<y_train.size();i++){
		for(int j = 0;j<10;j++){
			if(y_train[i]==j){
				temp_result.push_back(1);
			}else{
				temp_result.push_back(0);
			}
		}
		correct_result.push_back(temp_result);
		temp_result.clear();
	} 
//-------------------------------------------------------------


        //init the weight matrix X1
        float *X1; /* matrix */
        int m = 10, n = 784;
        X1 = new float[m*n];
        //m = 10 = row, n = 784 = col
        Mat_Init(m, n, X1);
        Mat_Show(m,n,X1);

        float *v;
        v = new float[784];
        for(int i = 0;i<X_train[0].size();i++){
            v[i] = X_train[0][i];
    
        }

        for(int a = 0; a<784;a++){
            cout<<v[a] << " ";
        }
  //-------------------------------------------------------  
        //bias vector B1 for hidden layer Y1
        float *B1; /* vector */
        B1 = new float[10];
        Bias_Init(10,B1); 

        //first hidden layer 
        float *Y1;
        Y1 = new float[10];// 10 neruon in 1st hidden layer
        Vec_Init(10,Y1);

        Mat_Xv(m, n, X1, Y1, v);
        Vec_Show(10,Y1);

        /* add two vectors */
        Vec_Add(10, Y1, B1, Y1);

        // float *temp1 = sigmoid(Y1);
        for(int i = 0;i<10;i++){
            Y1[i] = sigmoid(Y1[i]);
        }
        Vec_Show(10,Y1);
//---------------------------------------------------------------
        //bias vector B2 for hidden layer Y2
        float *B2; /* vector */
        B2 = new float[10];
        Bias_Init(10,B2); 

        //second hidden layer 
        float *Y2;
        Y2 = new float[10];// 10 neruon in 2nd hidden layer
        Vec_Init(10,Y2);

        //init the weight matrix X2
        float *X2; /* matrix */
        X2 = new float[10*10];
        //m = 10 = row, n = 10 = col
        Mat_Init(10, 10, X2);

        Mat_Xv(10, 10, X2, Y2, Y1);
        Vec_Show(10,Y2);
        Vec_Add(10, Y2, B2, Y2);
        // float *temp2 = sigmoid(Y2);
        for(int i = 0;i<10;i++){
            Y2[i] = sigmoid(Y2[i]);
        }
        Vec_Show(10,Y2);
//--------------------------------------------------------------
        //bias vector B3 for output layer O1
        float *B3; /* vector */
        B3 = new float[10];
        Bias_Init(10,B3); 

        //output layer 
        float *O1;
        O1 = new float[10];// 10 neruon in 1st hidden layer
        Vec_Init(10,O1);

        //init the weight matrix X3
        float *X3; /* matrix */
        X3 = new float[10*10];
        //m = 10 = row, n = 10 = col
        Mat_Init(10, 10, X3);
       
        Mat_Xv(10, 10, X3, O1, Y2);
        Vec_Show(10,O1);
        Vec_Add(10, O1, B3, O1);
        // float *temp3 = sigmoid(O1);
        for(int i = 0;i<10;i++){
            O1[i] = sigmoid(O1[i]);
        }
        Vec_Show(10,O1);

    cout <<"Ground truth : \n";
    for (int i = 0; i < 1;i++)
	{
		for (int p = 0; p < correct_result[i].size();p++)
		{
			cout<<correct_result[i][p]<<" ";
		}
		cout<<"\n";
	}
//----------------------------------------------------
//saving the target result
    float *R1; /* vector */
    R1 = new float[10];
	for (int p = 0; p < correct_result[0].size();p++)
	{
		R1[p] = correct_result[0][p];
	}
    Vec_Show(10,R1);

    float *C1_O1; /* vector */
    C1_O1 = new float[10];

    /* sub two vectors */
    //x and y are input, z is output
    Vec_Sub(10,O1, R1 , C1_O1);
    Vec_Show(10,C1_O1);


    //signmoid der
        //init the weight vector TEST
        float *TEST; /* matrix */
        TEST = new float[10];   

    for(int i = 0;i<10;i++){
        TEST[i] = sigmoid_derivative(O1[i]);
    }
        
    Vec_Show(10,TEST);

    
    //signmoid der
    //init the weight vector TEST
    float TEST2 = Dot_Product(10, C1_O1, TEST);
    cout<<TEST2<<endl;




    delete[] X1;
    delete[] X2;
    delete[] X3;

	delete[] Y1;
    delete[] Y2;
	
    delete[] v;
    delete[] O1;

    delete[] C1_O1;
    delete[] R1;


;


	return 0;
}