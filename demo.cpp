#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include <sstream>
#include <string>
#include <random>
#include <time.h>
#include <windows.h>
#include <algorithm>
#include <cmath> 



using namespace std;

//generate the weight between -1 to 1 
double weightRand() {
  return ((float(rand()) / float(RAND_MAX)) * (1 - (-1))) +(-1);
}

float sigmoid(float value){
	return 1 / (1 + exp(-1 * value));
}

float sigmoid_derivative(float value){
	return sigmoid(value) * (1 - sigmoid(value));
}

void update(float learning_rate,vector<float> cost,vector<vector<float>> &weight_matrix){
	
}

vector<float> dot (vector<float> input_vector,vector<vector<float>> hidden_layer_weight,vector<float> hidden_layer_bias,float hidden_layer_size){
	vector<float> temp_hidden_layer;
	temp_hidden_layer.clear();
	float sum = 0.0;
	for(int i = 0; i < hidden_layer_size;i++){	
		for(int z = 0; z < input_vector.size();z++){
			sum = sum + (input_vector[z]* hidden_layer_weight[i][z]);
		}
		temp_hidden_layer.push_back(sigmoid(sum + hidden_layer_bias[i]));
		sum = 0.0;
	}
	return temp_hidden_layer;
}

vector<float> cal_cost(vector<float> last_hidden_vector, vector<float> result_vector){

	vector<float> delta;
	for (int i = 0; i < result_vector.size();i++)
	{
		delta.push_back(pow((last_hidden_vector[i] - result_vector[i]),2.0));
	}

	return delta;
}

 vector<vector<float>> Weight_Transpose(vector<vector<float>> matrix){
	vector<vector<float>> temp_matrix;
	vector<float> temp_vector;

	for(unsigned int i=0;i<matrix[0].size();i++){
    		for(unsigned int j=0;j<matrix.size();j++){
       			temp_vector.push_back(matrix[j][i]);
    	}
		temp_matrix.push_back(temp_vector);
		temp_vector.clear();
	}

	return temp_matrix;

}

vector<float> Vector_Transpose(vector<float> vector){

}

 dot_product(vector<float> hidden_layer_vector,vector<float> error_vector){



}



// void matrix_vector_subtraction(vector<vector<float>> &weight_matrix, vector <float> loss_vector){
    
//     /*  Returns the difference between two vectors.
//      Inputs:
//      m1: vector
//      m2: vector
//      Output: vector, m1 - m2, difference between two vectors m1 and m2.
//      */
     
//     for (unsigned i = 0; i != VECTOR_SIZE; ++i){
//         difference[i] = m1[i] - m2[i];
//     };
    
//     return difference;
// }

int main(int argc, const char * argv[]) {


	//total input element
	vector< vector<float> > X_train;
	// correct value of each element 
	vector<float> y_train;


	ifstream myfile("train_small.txt");

	if (myfile.is_open())
	{
		cout << "Loading data ...\n";
		string line;
		while (getline(myfile, line))
		{
			int x, y;
			vector<float> X;
			stringstream ss(line);
			ss >> y;
			y_train.push_back(y);
			for (int i = 0; i < 28 * 28; i++) {
				ss >> x;
				X.push_back(x/255.0);
			}
			X_train.push_back(X);
		}

		myfile.close();
		cout << "Loading data finished.\n";
	} 
	else
		cout << "Unable to open file" << '\n';
	

	vector<vector<float>> correct_result;
	vector<float> temp_result;

	for(int i = 0;i<y_train.size();i++){
		for(int j = 0;j<10;j++){
			if(y_train[i]==j){
				temp_result.push_back(1);
			}else{
				temp_result.push_back(0);
			}
		}
		correct_result.push_back(temp_result);
		temp_result.clear();
	}

	//setting the learning rate
	float lr = 0.01;


	// size of input layer = 784
	int input_layer_size = 784;

 	//initialize size of hidden layer = numboer of neuron , should be input by user
	int hidden_layer_size = 10; 

	// size of output layer = 10
	int output_layer_size = 10;

	//initialize vector size and value to save the bias of hidden layer 1
	//default value of all bias set to 0
	vector<float> hidden_layer_1_bias(hidden_layer_size,2.0);

	vector<float> hidden_layer_2_bias(hidden_layer_size,2.0);
	
	//initialize vector size and value to save the weight of hidden layer 1
	vector< vector<float> > hidden_layer_1_weight; //default size is 10
	vector<float> hidden_layer_1_each_weight;//default size is 784

	for (int i = 0; i < hidden_layer_size;i++)
	{
		for (int p = 0; p < input_layer_size;p++)
		{
			hidden_layer_1_each_weight.push_back(weightRand());
		}
		hidden_layer_1_weight.push_back(hidden_layer_1_each_weight);
		hidden_layer_1_each_weight.clear();
	}


	// for (int i = 0; i < hidden_layer_1_weight.size();i++)
	// {
	// 	for (int p = 0; p < hidden_layer_1_weight[i].size();p++)
	// 	{
	// 		cout<<hidden_layer_1_weight[i][p]<<" ";
	// 	}
	// 	cout<<"\n";
	// }
	cout << "Row :" <<hidden_layer_1_weight.size()<< endl;
	cout << "Col :" <<hidden_layer_1_weight[0].size()<< endl;

	vector< vector<float> > transposed_weight_matrix = Weight_Transpose(hidden_layer_1_weight);



	// //initialize vector size and value to save the weight of hidden layer 2
	// vector< vector<float> > hidden_layer_2_weight; //default size is 10
	// vector<float> hidden_layer_2_each_weight;//default size is 784

	// for (int i = 0; i < hidden_layer_size;i++)
	// {
	// 	for (int p = 0; p < input_layer_size;p++)
	// 	{
	// 		hidden_layer_2_each_weight.push_back(weightRand());
	// 	}
	// 	hidden_layer_2_weight.push_back(hidden_layer_2_each_weight);
	// 	hidden_layer_2_each_weight.clear();
	// }

	// //a vector that show the hidden layer result
	// vector<float> hidden_layer_1;
	// vector<float> hidden_layer_2;
	// vector<float> output_layer;
	// vector<float> total_delta;

	// float Avg_total_error = 0.0;
	// for (int i = 0; i < X_train.size();i++)
	// {
	// 	hidden_layer_1 = dot(X_train[i],hidden_layer_1_weight,hidden_layer_1_bias,hidden_layer_size);
	// 	hidden_layer_2 = dot(hidden_layer_1,hidden_layer_2_weight,hidden_layer_2_bias,hidden_layer_size);

	// 	cout <<endl;

	// 	// Back propagation
    //     total_delta = cal_cost(hidden_layer_2, correct_result[i]);

		
	// 	for(int z = 0; z < total_delta.size();z++){
	// 		Avg_total_error = Avg_total_error + total_delta[z];
	// 	}
		
	// 	Avg_total_error = Avg_total_error * (1 / 2);
	// 	cout << "The average error in this epoch is " << Avg_total_error << " ";


	// 	//update the weight
	// 	//multiply the learning and the Avg loss
	// 	float temp_update_weight;
	// 	temp_update_weight = lr * Avg_total_error;


	// }


	// cout << endl;
	// for(int z = 0; z < hidden_layer_2.size();z++){
	// 	cout <<hidden_layer_2[z] << " ";
	// }


	return 0;
}

