//
//  NeuraNet.cpp
//  2.Milestone
//
//  Created by Eric Walter on 17.10.19.
//  Copyright Â© 2019 Eric Walter. All rights reserved.
//

#include "NeuraNet.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <math.h>
#include <assert.h>

using namespace std;

/**
 Constructor for a neuron object
 */
Neuron::Neuron(unsigned numOutputs, unsigned myIndex){
    
    // loop through connections
    for(unsigned connection = 0; connection < numOutputs; ++connection){
        m_outputWeights.push_back(Connection());
        
        // assign random weight
        m_outputWeights.back().weight = randomWeight();
        
        // set weight to zero
       // m_outputWeights.back().weight = 0;
    }
    // set index
    m_myIndex = myIndex;
    
    // set alpha
    setAlpha(0.5);
    // set learning rate
    setLearnRate(0.1);
}

/**
 Function to return a random number for the weights
 */
float Neuron::randomWeight(void){
    return rand() / float(RAND_MAX);
}

/**
 Function to return a transfer value calculated with the sigmoid function f(x) = 1/(1 + e^-x)
 */
float Neuron::transFunct(float x){
    
    // we want sigmoid function
    
    return 1 / (1 + exp(-x));
    
    // tanh - output range [-1.0...1.0]
    // return tanh(x);
}

/**
 Function to return the derivative of a transfer with the sigmoid function derivative f'(x) = f(x)(1 - f(x))
 */
float Neuron::transFunctDerivat(float x){
    
    // we want sigmoid function derivative
    return x * (1 - x);
    
    // tanh derivative
    // return 1 - pow(x,2);
}

/**
 Feed forward function of the neuron
 */
void Neuron::feedForward(const Layer &prevLayer){
    float sum = 0;
    // sum the previous layer's outputs (which are our inputs)
    // include the bias node from the previous layer
    
    for(unsigned neuron = 0; neuron < prevLayer.size(); ++neuron){
        sum += prevLayer[neuron].getOutputVal() * prevLayer[neuron].m_outputWeights[m_myIndex].weight;
    }
    
    // use transfer function to calculate output
    m_outputVal = transFunct(sum);
}

/**
 Function to calculate output gradients of a neuron
 */
void Neuron::calcOutputGradients(float targetVal){
    // calculate difference between values
    float delta = targetVal - m_outputVal;
    // multiply difference by derivative of output value
    m_gradient = delta * Neuron::transFunctDerivat(m_outputVal);
}

/**
 Function to sum dow of hidden layers
 */
float Neuron::sumDOW(const Layer &nextlayer) const{
    float sum = 0;
    // sum our contributions of the errors of the nodes we feed
    for(unsigned neuron = 0; neuron < nextlayer.size() - 1; ++neuron){
        sum += m_outputWeights[neuron].weight * nextlayer[neuron].m_gradient;
    }
    return sum;
}

/**
 Function to calculate hidden gradients
 */
void Neuron::calcHiddenGradients(const Layer &nextLayer){
    float dow = sumDOW(nextLayer);
    m_gradient = dow * Neuron::transFunctDerivat(m_outputVal);
}

/**
 Function to update input weights of a neuron
 */
void Neuron::updateInputWeights(Layer &prevLayer){
    // the weights to be updated are in the connection container in the neurons of the previous layer
    
    // go through all neurons including the bias
    for(unsigned neuron = 0; neuron < prevLayer.size(); ++neuron){
        Neuron &other_neuron = prevLayer[neuron];
        float oldDeltaWeight = other_neuron.m_outputWeights[m_myIndex].deltaWeight;
        // new delta equals learning rate multiplied by output value times gradient times momentum(aplha)
        float newDeltaWeight =
            m_learnRate
            * other_neuron.getOutputVal()
            * m_gradient
            // momentum is a fraction of the previous delta weight
            + m_alpha
            * oldDeltaWeight;
        other_neuron.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
        other_neuron.m_outputWeights[m_myIndex].weight += newDeltaWeight;
    }
}

///_________________________________________________________________________________________________________________

/*
 Constructor of a neural net object
 */
NeuralNet::NeuralNet(const vector<unsigned> &topology){
    unsigned numLayers = topology.size();
    // loop to get layers
    for(unsigned layerNum = 0; layerNum < numLayers; ++layerNum){
        // creating a new layer object and adding it to the vector
        m_layers.push_back(Layer());
        // define variable for number of outputs for each neuron
        // if it's the output layer, then output is zero otherwise what is in next topology element
        unsigned numOutputs = (layerNum == (topology.size() - 1) ? 0 : topology[layerNum + 1]);
        
        // loop to fill fill layers with neurons: <
        // and add an extra bias neuron: <=
        for(unsigned neuroNum = 0; neuroNum <= topology[layerNum]; ++neuroNum){
            // get most recent added element
            m_layers.back().push_back(Neuron(numOutputs, neuroNum));
            cout << "Created a new Neuron!" << endl;
        }
        
        // Force the bias node's output value to 1.0. It's the last neuron created above
        m_layers.back().back().setOutputVal(1.0);
    }
}

/*
 Empty destructor
*/
NeuralNet::~NeuralNet(){}

/*
 Function to print a vector
 */
void NeuralNet::printVector(vector<float> X){
    for(vector<float>::iterator it = X.begin(); it != X.end(); it++ ){
        cout << *it << " ";
    }
    cout << endl;
}

void NeuralNet::feedForward(const vector<float> &y)
{
    // check if number of input values equal number of input neurons
    assert(y.size() == (m_layers[0].size() - 1));
    
    // latch the input values into the input neurons
    for(unsigned input = 0; input < y.size(); ++input){
        m_layers[0][input].setOutputVal(y[input]);
    }
    
    // forward propagation
    for(unsigned layerNum = 1; layerNum < m_layers.size(); ++layerNum){
        Layer &prevLayer = m_layers[layerNum - 1];
        for(unsigned neuron = 0; neuron < (m_layers[layerNum].size() - 1) ; ++neuron){
            m_layers[layerNum][neuron].feedForward(prevLayer);
        }
    }
}

/**
 Function for back propagation
 */
void NeuralNet::backProp(const vector<float> &targetVals){
    // Calculate overall net error [RMS(root mean square error) of output neuron errors]
    
    Layer &outputLayer = m_layers.back();
    m_error = 0;
    // loop through neurons, not including the bias
    for(unsigned neuron = 0; neuron < (outputLayer.size() - 1); ++neuron){
        // delta equal target minus the actual value
        float delta = targetVals[neuron] - outputLayer[neuron].getOutputVal();
        // sum up delta squared
        m_error += pow(delta,2);
    }
    // divide number of summed up elements to get average error squared
    m_error /= outputLayer.size() - 1;
    // take square root to get RMS
    m_error = sqrt(m_error);
    
    // implement a recent average error measurement
    
    m_recAvgErr = (m_recAvgErr * m_recAvgErrSmoothFac + m_error)/(m_recAvgErrSmoothFac + 1.0);
    
    // Calculate output layer gradients
    // loop through all neuron in output layer ecxept bias
    for(unsigned neuron = 0; neuron < outputLayer.size() - 1; ++neuron){
        outputLayer[neuron].calcOutputGradients(targetVals[neuron]);
    }
    
    // Calculate hidden layer gradients
    
    for(unsigned layerNum = m_layers.size() - 2; layerNum > 0; --layerNum){
        Layer &hiddenLayer = m_layers[layerNum];
        Layer &nextLayer = m_layers[layerNum + 1];
        
        // loop through all elements in hidden layer
        for(unsigned neuron = 0; neuron < hiddenLayer.size(); ++neuron){
            // for each hidden layer calculate the gradien
            hiddenLayer[neuron].calcHiddenGradients(nextLayer);
        }
    }
    
    // for all layer from outputs to first hidden layer, update connection weights
    // starting at the right side of layer until before input layer
    for(unsigned layerNum = m_layers.size() - 1; layerNum > 0; --layerNum){
        Layer &layer = m_layers[layerNum];
        Layer &prevLayer = m_layers[layerNum - 1];
        
        // index individual neuron and update it's input weights
        for(unsigned neuron = 0; neuron < layer.size(); ++neuron){
            layer[neuron].updateInputWeights(prevLayer);
        }
    }
}

/**
 Function to get results
 */
void NeuralNet::getResults(vector<float> &resultVals) const{
    // clear the passed on container
    resultVals.clear();
    for(unsigned neuron = 0; neuron < m_layers.back().size() -1 ; ++neuron){
        resultVals.push_back(m_layers.back()[neuron].getOutputVal());
    }
}

/**
 Function to export weights in a file
 */
void NeuralNet::exportWeights(string filename)
{
    ofstream outputFile;
    outputFile.open(filename);
    for (int i = 0; i < m_weight.size(); i++)
    {
        outputFile << m_weight[i] << endl;
    }
    outputFile.close();
}

/**
 Function to import weights from a file
 */
void NeuralNet::importWeights(string filename)
{
    ifstream inputFile;
    inputFile.open(filename);

    for (int i = 0; i < m_weight.size(); i++)
    {
        inputFile >> m_weight[i];
    }
    inputFile.close();
}

/**
 Constructor for training data object
 */
TrainingData::TrainingData(const string filename)
{
    m_trainingDataFile.open(filename.c_str());
}
/**
 Method to load training data from file and save it in vectors
 */
unsigned TrainingData::getData(vector<float> &inputVals, vector<float> &targetOutputVals){
    targetOutputVals.clear();
    inputVals.clear();
    if(m_trainingDataFile.is_open()){
        cout << "Loading data ...\n";
        string line;
        getline(m_trainingDataFile, line);
        stringstream ss(line);
        
        float label, value;
        ss >> label;
        targetOutputVals.push_back(label);
        
        for (int i = 0; i < 28 * 28; i++) {
            ss >> value;
            inputVals.push_back(value/255.0);
        }
        
    }else
        cout << "Unable to open file!\n";
    
    return inputVals.size();
}
