//
//  NeuraNet.hpp
//  2.Milestone
//
//  Created by Eric Walter on 17.10.19.
//  Copyright Â© 2019 Eric Walter. All rights reserved.
//

#ifndef NeuraNet_hpp
#define NeuraNet_hpp

#include <stdio.h>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;

/**
 Neuron class as forward reference for type definition
 */
class Neuron;
// type definition of private variable Layer
typedef vector<Neuron> Layer;

/**
 Structure for the connection between neurons
 */
struct Connection{
    float weight;
    float deltaWeight;
};

/**
 Definition of Neuron class
 */
class Neuron{
public:
    // constructor for neuron
    Neuron(unsigned numOutputs, unsigned myIndex);
    
    // setter function for output value
    void setOutputVal(float val){ m_outputVal = val; };
    
    // getter function for output value
    float getOutputVal(void) const { return m_outputVal; };
    
    // feed forward function of neuron
    void feedForward(const Layer &prevLayer);
    
    // calculate output gradient function
    void calcOutputGradients(float targetVal);
    
    // calculate hidden gradients function
    void calcHiddenGradients(const Layer &nextLayer);
    
    // update weights
    void updateInputWeights(Layer &prevLayer);
    
    // setter for learning rate
    void setLearnRate(float learnRate){ m_learnRate = learnRate; };
    
    // setter for momentum
    void setAlpha(float alpha){ m_alpha = alpha; };
    
private:
    // output values of a neuron
    float m_outputVal;
    
    // gradient value of neuron
    float m_gradient;
    
    // learning rate
    float m_learnRate;
    
    // momentum
    float m_alpha;
    
    // output weights of a neuron
    vector<Connection> m_outputWeights;
    
    // function to generate random weights
    float randomWeight(void);
    
    // index variable of a neuron
    unsigned m_myIndex;
    
    // transfer function
    float transFunct(float x);
    
    // transfer function derivative
    float transFunctDerivat(float x);
    
    // function to sum dow of hidden layer
    float sumDOW(const Layer &nextlayer) const;
};

// *************** Neural Net Class ************************************
/**
 Class representing a neural network. The simplest version without hidden layers is called Perceptron
 */
class NeuralNet
{
public:
    // constructor for neural net
    NeuralNet(const vector<unsigned> &topology);
    
    // destructor for neural net
    ~NeuralNet();
    
    // function for back propagation
    void backProp(const vector<float> &targetVals);
    
    // function to get the result values
    void getResults(vector<float> &resultVals) const;
    
    // function to print a vector
    void printVector(vector<float>);
    
    // setter for learning rate
    void setLearnRate(float learnRate){ m_learnRate = learnRate; };
    
    // setter for epochs
    void setEpochs(int epochs){ m_epochs = epochs; };
    
    // function to feed forward
    void feedForward(const vector<float> &y);
    
    // function to export weights
    void exportWeights(string filename);
    
    // function to import weights
    void importWeights(string filename);
    
    // function to print weights
    void printWeights();
    
    // getter function for recent average error
    float getRecentAverageError(void) const { return m_recAvgErr; }
    
// private member variables
private:

    // learning rate
    float m_learnRate;
    
    // epochs
    int m_epochs;
    
    // weights
    vector < float > m_weight;
    
    // error variable
    float m_error;
    
    // recent average error
    float m_recAvgErr;
    
    // recent average smoothing factor
    float m_recAvgErrSmoothFac = 100.0;
    
    // layer is a vector of neurons
    vector<Layer> m_layers; // m_layers[layerNum][neuroNum]
};

/**
 Class representing the handling of training data
 */
class TrainingData
{
public:
    // constructor
    TrainingData(const string filename);
    
    // function to check if end-of-file is reached
    bool isEof(void) { return m_trainingDataFile.eof(); }
    
    // function to get the topology of the data
    void getTopology(vector<unsigned> &topology);

    // Returns the number of input values read from the file:
    unsigned getNextInputs(vector<float> &inputVals);
    unsigned getTargetOutputs(vector<float> &targetOutputVals);
    unsigned getData(vector<float> &inputVals, vector<float> &targetOutputVals);

private:
    // variable containing the streamed data
    ifstream m_trainingDataFile;
};

#endif /* NeuraNet_hpp */
