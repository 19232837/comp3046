//
//  Advanced Programming for Software Developement
//  Department of Computer Science
//  Hong Kong Baptist University
//
//  Student-ID: 19502435
//  Name: Eric Walter
//
//  main.cpp
//  2.Milestone
//
//  Created by Eric Walter on 11.10.19.
//  Copyright Â© 2019 Eric Walter. All rights reserved.
//

#include "NeuraNet.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <math.h>
#include <time.h>
#include <assert.h>

using namespace std;


// TODO: - change to mini-batch SGD (gradient)
// TODO: - save and load weights
// TODO: - add openMP methods

/**
 Function to show the vector values
 */
void showVectorVals(string label, vector<float> &v)
{
    cout << label << " ";
    for (unsigned i = 0; i < v.size(); ++i) {
        cout << v[i] << " ";
    }
    cout << endl;
}

int main()
{
    // provided data load function -------------------
//    vector< vector<float> > X_train;
//    vector<float> y_train;
//
//    ifstream myfile("train_small.txt");
//
//    if (myfile.is_open())
//    {
//        cout << "Loading data ...\n";
//        string line;
//        while (getline(myfile, line))
//        {
//            int x, y;
//            vector<float> X;
//            stringstream ss(line);
//            ss >> y;
//            y_train.push_back(y);
//            for (int i = 0; i < 28 * 28; i++) {
//                ss >> x;
//                X.push_back(x/255.0);
//            }
//            X_train.push_back(X);
//        }
//
//        myfile.close();
//        cout << "Loading data finished.\n";
//    }
//    else
//        cout << "Unable to open file" << '\n';
    //---------------------------------------------------
    
    // create topology for neural net
    vector<unsigned> topology;
    
    // Input Layer with 784 neurons
    topology.push_back(784);
    
    // Hidden layers with 20 neurons
    topology.push_back(20);
    
    // Output layer with 10 neurons (0 - 9)
    topology.push_back(10);
    
    TrainingData trainData("train_small.txt");
    
    // remember the time in order to calculate processing time at the end
    time_t startTime = time(NULL);
    
    // create neural net object with given topology
    NeuralNet myNet(topology);

    vector<float> targetVals, inputVals, resultVals;

    // initialize epochs to zero
    int trainingPass = 0;
    
    while (!trainData.isEof()){
        ++trainingPass;
        cout << endl << "Training pass " << trainingPass << endl;
    
        // Get new input data and feed it forward:
//        if (trainData.getNextInputs(inputVals) != topology[0]) {
//            break;
//        }
        if (trainData.getData(inputVals,targetVals) != topology[0]) {
            break;
        }

        showVectorVals("Inputs:", inputVals);
        myNet.feedForward(inputVals);

        // Collect the net's actual output results:
        myNet.getResults(resultVals);
        showVectorVals("Outputs:", resultVals);

        // Train the net what the outputs should have been:
       // trainData.getTargetOutputs(targetVals);
        showVectorVals("Target:", targetVals);
     //   assert(targetVals.size() == topology.back());
        
        // Start backpropagation
        myNet.backProp(targetVals);

        // Report how well the training is working, average over recent samples:
        cout << "Net recent average error: "
                << myNet.getRecentAverageError() << endl;
    }
    
    // Calculate and print the program's total execution time
    time_t endTime = time(NULL);
    double executionTime = difftime(endTime, startTime);
    printf("\n    DONE! Total execution time: %.1f sec\n\n",executionTime);
    
    
    
}
